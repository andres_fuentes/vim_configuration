syntax on
set number
set nowrap
set tabstop=4
set shiftwidth=4 
set softtabstop=4 
set expandtab
set rnu
set smartindent
set clipboard=unnamed
set bg=dark
colorscheme monokai_darker 

"Variables para el explorador de archivos
let g:netrw_browse_split = 3 "Abre documento en una pestania nueva
let g:netrw_winsize = 25 "Porcentaje que ocupa el explorador
